# ZPRO 2024

Tato stránka bude obsahovat veškeré informace týkající se výuky předmětu 18ZPRO – oznámení, materiály ke cvičení, apod.

Obsah tohoto projektu můžete jednoduše synchronizovat do prostředí [JupyterHub](https://jupyter.fjfi.cvut.cz/) kliknutím na následující tlačítko:

[![Static Badge](https://img.shields.io/badge/Synchronizovat%20na%20JupyterHub-08c?logo=jupyter&labelColor=white&color=08c)](
https://jupyter.fjfi.cvut.cz/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.fjfi.cvut.cz%2Fksi%2Fzpro-2024-public.git&urlpath=lab%2Ftree%2Fzpro-2024-public.git%2FREADME.md&branch=main)

## Oznámení

- V úterý 1. října od 16:00 budou výjimečně obě skupiny spojeny v učebně T-101.
- V pondělí 21. října a v úterý 22. října se na cvičení bude psát první test.
  Standardní doba na vypracování bude 15 minut a bude obsahovat 12 jednoduchých otázek z témat, která jsme dosud stihli probrat.
  Za každou otázku bude možné získat 0-10 bodů, ale za celý test je maximum 100 bodů (dvě otázky jsou bonus, kde je možné získat ztracené body).
  Ukázkovou verzi otázek najdete v souboru [testy/demo1.pdf](testy/demo1.pdf).
  Test bude mít papírovou formu a nebude při něm možné používat jakékoliv elektronické pomůcky.
- Odevzdání 3. úkolu je prodlouženo do středy 23. října, 23:59.
- Na stránce https://gitlab.fjfi.cvut.cz/ksi/zpro-2024-resene-priklady najdete řešené příklady ze cvičení.
- V pondělí 25. listopadu a v úterý 26. listopadu se na cvičení bude psát druhý test.
  Forma testu bude stejná jako v případě prvního testu.
  Ukázkovou verzi otázek najdete v souboru [testy/demo2.pdf](testy/demo2.pdf).
- Ve středu 11.12., ve čtvrtek 12.12. a v pátek 13.12. se na cvičení bude psát
  třetí (poslední) test. Forma testu bude stejná jako v předchozích případech.
  Ukázkovou verzi otázek najdete v souboru [testy/demo3.pdf](testy/demo3.pdf).

## Odkazy

- [Bílá kniha](https://bilakniha.cvut.cz/cs/predmet11274505.html) – osnova, doplňující literatura
- [GitLab projekt](https://gitlab.fjfi.cvut.cz/ksi/zpro-2024-public)
- [Úvodní přednáška z Přípravného týdne](00_PT.pdf), [dotazník](https://docs.google.com/forms/d/e/1FAIpQLSeXYprNt5VlgPLzJyPenNRKlAtz9BSRlh0n80pcOTXfzsDGhQ/viewform)
- [Informace o uživatelském účtu ČVUT](https://it.fjfi.cvut.cz/navody/uzivatelsky-ucet/ucet-cvut)
- [FJFI JupyterHub](https://jupyter.fjfi.cvut.cz/)
- [Anonymní prostředí Jupyter](https://jupyter.org/try-jupyter)
- [Řešené příklady ze cvičení](https://gitlab.fjfi.cvut.cz/ksi/zpro-2024-resene-priklady)

## Autoři

Na přípravě těchto a [předchozích](https://gitlab.fjfi.cvut.cz/ksi/zpro-2023-public) materiálů se podíleli:

- Jakub Klinkovský
- Zuzana Petříčková
- Vladimír Jarý
- Maksym Dreval
- Petr Pauš
- Jan Tomsa
- František Voldřich

Materiály jsou dostupné pod licencí [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
